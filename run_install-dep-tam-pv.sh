#!/bin/bash

## you will need to enter your root password during this script execution
## at some points, you will be prompted to choose options: always press ENTER (this will select default option)

## ipv6 configuration may cause installation to fail
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1

## dependencies
sudo apt remove -y needrestart 
sudo add-apt-repository --remove ppa:avsm/ppa -y
sudo apt update -y
sudo apt full-upgrade -y
sudo apt install software-properties-common -y
sudo apt install time libgtk2.0-dev pkg-config opam curl make unzip graphviz build-essential bubblewrap parallel python3-pip locales -y

## sympy
pip3 install sympy

### haskell
sudo -- sh -c -e "echo '\n#haskell\n185.199.110.133 raw.githubusercontent.com\n151.101.245.175 downloads.haskell.org' >> /etc/hosts"
curl -sSL https://get.haskellstack.org/ | sh -s - -f

### maude
curl -L https://github.com/SRI-CSL/Maude/releases/download/Maude3.3.1/Maude-linux.zip > maude.zip
unzip -oj maude.zip -d ~/.local/bin/
mv -f ~/.local/bin/maude.linux64 ~/.local/bin/maude
chmod a+x ~/.local/bin/maude
rm maude.zip

## update PATH
echo 'export PATH="$HOME/.local/bin/:$PATH"' >> ~/.bashrc
export PATH="$HOME/.local/bin/:$PATH"
. ~/.bashrc
echo "PATH=$PATH"

## tamarin
git clone https://github.com/tamarin-prover/tamarin-prover.git
cd tamarin-prover
git checkout master
make default
cd ..


## proverif
bash -c "sh <(curl -fsSL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)"
opam init
opam update 
opam install "proverif=2.04"
