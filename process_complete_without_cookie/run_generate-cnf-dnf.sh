#!/bin/sh

JOBS=250

echo "Generate CNF and DNF files"

mkdir results

time -p -f "%E" parallel --jobs $JOBS < wireguard_command_generate_cnf_dnf
