#!/usr/bin/env python3.6

import os
import itertools

from subprocess import PIPE, Popen

from compute_proof import tamarin_proverif_agreement_confirm

from multiprocessing import Pool 

EMPTY="../process_empty/"
QUERIES="../queries/"

INTRO=EMPTY+"wireguard_intro.spthy"

MACRO="wireguard_macro.spthy"

END=EMPTY+"wireguard_end.spthy"

AGREEMENT_SPTHY="agreement_confirm/wireguard_agreement_macro.spthy"

AGREEMENT_QUERIES=QUERIES+"wireguard_agreement_confirm_export_queries.spthy"


keys = ["ltki", "ltkr", "pki", "pkr", "eki", "ekr", "precomp_i", "precomp_r", "psk"]

L1c = list(itertools.combinations(keys, 1))
L2c = list(itertools.combinations(keys, 2))
L3c = list(itertools.combinations(keys, 3))
L4c = list(itertools.combinations(keys, 4))
L5c = list(itertools.combinations(keys, 5))
L6c = list(itertools.combinations(keys, 6))
L7c = list(itertools.combinations(keys, 7))
L8c = list(itertools.combinations(keys, 8))
L9c = list(itertools.combinations(keys, 9))



path = "agreement_confirm"

if os.path.exists(path):
	os.chdir(path)
	for file in sorted(os.listdir()):
		os.remove(file)
	os.chdir("..")
	os.rmdir(path)
if not os.path.exists(path):
	os.makedirs(path)


listfiles=[INTRO, MACRO, AGREEMENT_QUERIES, END]

with open(AGREEMENT_SPTHY, "w") as outfile:
	for file in listfiles:
		with open(file, "r") as infile:
			outfile.write(infile.read())


os.chdir(path)

tamarin_proverif_agreement_confirm("all_trusted")

for l in L1c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0])

for l in L2c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]) 

for l in L3c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2])

for l in L4c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3])

for l in L5c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4])

for l in L6c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5])

for l in L7c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6])

for l in L8c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6]+"_"+l[7])

for l in L9c :
	tamarin_proverif_agreement_confirm("untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6]+"_"+l[7]+"_"+l[8])