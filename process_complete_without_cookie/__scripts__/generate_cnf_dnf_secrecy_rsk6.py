#!/usr/bin/env python3.6

import subprocess
import itertools
import os
import multiprocessing
import time

import compute_dnf_cnf

from sympy.logic.boolalg import to_dnf
from sympy.logic.boolalg import to_cnf
from sympy import symbols
from sympy import simplify

from multiprocessing import Pool 

def cnf(data):
	return to_cnf(data, simplify=True, force=True)

def dnf(data):
	return to_dnf(data, simplify=True, force=True)

def star(data):
	datastar_inter = data+" & (Mu >> Ru) & (Mv >> Rv) & (Mx >> Rx) & (My >> Ry) & (Mi >> Rc) & (Mr >> Rc) & (Ms >> Rs)"
	dnf_inter = str(dnf(datastar_inter))
	datastar = dnf_inter.replace(" & ~Ms", "")
	datastar = datastar.replace(" & ~Mu", "")
	datastar = datastar.replace(" & ~Mv", "")
	datastar = datastar.replace(" & ~Mx", "")
	datastar = datastar.replace(" & ~My", "")
	datastar = datastar.replace(" & ~Mi", "")
	datastar = datastar.replace(" & ~Mr", "")
	dnf_final = dnf(datastar)
	return dnf_final

st = time.time()

pattern = "RESULT"

#keys = ["pki", "pkr"]
keys = ["ltki", "ltkr", "pki", "pkr", "eki", "ekr", "precomp_i", "precomp_r", "psk"]
#keys = ["ltki", "ltkr", "pki", "pkr", "eki", "precomp_i", "precomp_r"]

L1c = list(itertools.combinations(keys, 1))
L2c = list(itertools.combinations(keys, 2))
L3c = list(itertools.combinations(keys, 3))
L4c = list(itertools.combinations(keys, 4))
L5c = list(itertools.combinations(keys, 5))
L6c = list(itertools.combinations(keys, 6))
L7c = list(itertools.combinations(keys, 7))
L8c = list(itertools.combinations(keys, 8))
L9c = list(itertools.combinations(keys, 9))


path = "secrecy_rsk6"

os.chdir(path)

pathdnfcnf = "all_dnf_cnf"

if os.path.exists(pathdnfcnf):
	os.chdir(pathdnfcnf)
	for file in sorted(os.listdir()):
		os.remove(file)
	os.chdir("..")
	os.rmdir(pathdnfcnf)
if not os.path.exists(pathdnfcnf):
	os.makedirs(pathdnfcnf)

pathtmp = "all_trusted"

output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
for file_name in sorted(os.listdir()):
	if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
		(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
		with open(file_name, "r") as input_file:
			for line in input_file.readlines():
				if 'RESULT' in line:
					output_file.write(line)
output_file.close()

if os.path.exists(pathtmp):
	os.chdir(pathtmp)
	for file in sorted(os.listdir()):
		os.remove(file)
	os.chdir("..")
	os.rmdir(pathtmp)
if not os.path.exists(pathtmp):
	os.makedirs(pathtmp)

os.chdir(pathtmp)

with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
	with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
	    for line in infile.readlines():
	        if 'RESULT' in line:
	            outfile.write(line)
outfile.close()
infile.close()


with open('prefix.txt', 'w') as outfile:
	outfile.write(pathtmp)
outfile.close()

filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

compute_dnf_cnf.main(filetmp)

if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
	with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
		with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
		    for line in infile.readlines():
		    	outfile.write(line)
	outfile.close()
	infile.close()



os.chdir("..")


for l in L1c :

	pathtmp = "untrusted_"+l[0]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)


	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()

	os.chdir("..")



for l in L2c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


for l in L3c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


for l in L4c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


for l in L5c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


for l in L6c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()



	os.chdir("..")


for l in L7c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")



for l in L8c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6]+"_"+l[7]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


for l in L9c :

	pathtmp = "untrusted_"+l[0]+"_"+l[1]+"_"+l[2]+"_"+l[3]+"_"+l[4]+"_"+l[5]+"_"+l[6]+"_"+l[7]+"_"+l[8]

	output_file = open("wireguard_secrecy_rsk6_"+pathtmp+".pv.log", "w")
	for file_name in sorted(os.listdir()):
		if ((file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_0") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_1") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_2") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_3") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_4") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_5") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_6") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_7") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_8") and file_name.endswith(".pv.log")) or \
			(file_name.startswith("wireguard_secrecy_rsk6_"+pathtmp+"_9") and file_name.endswith(".pv.log"))):
			with open(file_name, "r") as input_file:
				for line in input_file.readlines():
					if 'RESULT' in line:
						output_file.write(line)
	output_file.close()

	if os.path.exists(pathtmp):
		os.chdir(pathtmp)
		for file in sorted(os.listdir()):
			os.remove(file)
		os.chdir("..")
		os.rmdir(pathtmp)
	if not os.path.exists(pathtmp):
		os.makedirs(pathtmp)

	os.chdir(pathtmp)

	with open('wireguard_secrecy_rsk6_'+pathtmp+'.results', 'w') as outfile:
		with open('../wireguard_secrecy_rsk6_'+pathtmp+'.pv.log', 'r') as infile:
		    for line in infile.readlines():
		        if 'RESULT' in line:
		            outfile.write(line)
	outfile.close()
	infile.close()

	with open('prefix.txt', 'w') as outfile:
		outfile.write(pathtmp)

	filetmp = 'wireguard_secrecy_rsk6_'+pathtmp+'.results'

	compute_dnf_cnf.main(filetmp)

	if os.stat('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf').st_size !=0:
		with open('../'+pathdnfcnf+'/'+'wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'w') as outfile:
			with open('wireguard_secrecy_rsk6_'+pathtmp+'.results.cnf', 'r') as infile:
			    for line in infile.readlines():
			    	outfile.write(line)

		outfile.close()
		infile.close()


	os.chdir("..")


os.chdir(pathdnfcnf)

listfiles=sorted(os.listdir())



with open("cnf_dnf.txt", "w") as outfile:
	with open(listfiles[0], "r") as infile:
	    for line in infile.readlines():
	    	outfile.write("("+line+")")
	for file in listfiles[1:]:
		with open(str(file), "r") as infile:
		    for line in infile.readlines():
		    	outfile.write(" | ("+line+")")

Ru, Rv, Rx, Ry, Rc, Rs, Du, Dv, Mu, Mv, Mx, My, Ms, Mi, Mr = symbols("Ru, Rv, Rx, Ry, Rc, Rs, Du, Dv, Mu, Mv, Mx, My, Ms, Mi, Mr")

if os.stat("cnf_dnf.txt").st_size != 0:
	with open('../../results/wireguard_secrecy_rsk6.cnfdnf', 'w') as outfile:
		with open('cnf_dnf.txt', 'r') as infile:
			data = infile.readline()
			with Pool() as p:
				rescnf = p.map(cnf, [data])
				#print("CNF for agreement confirm = "+str(rescnf[0]))
				outfile.write("CNF for secrecy rsk6 = "+str(rescnf[0])+"\n")
				resdnf = dnf(str(rescnf[0]))
				#print("DNF for agreement confirm = "+str(resdnf))
				outfile.write("DNF for secrecy rsk6 = "+str(resdnf)+"\n")
				dnfstar = star(str(rescnf[0]))
				outfile.write("DNF* for secrecy rsk6 = "+str(dnfstar)+"\n")
				elapsed_time = time.time() - st
				outfile.write("Duration = "+time.strftime("%H:%M:%S", time.gmtime(elapsed_time))+"\n")
		infile.close()
		outfile.close()
#else:
	#print("CNF for agreement confirm = ")
	#print("DNF for agreement confirm = ")