/*
 * Protocol:    WireGuard protocol
 * Modeler:     Undisclosed
 * Date:        2023
 * Source:      Inspired from original WireGuard, IKpsk2 models
 * Status:      Complete
 *
 */




theory WireGuard

begin

builtins: hashing, diffie-hellman, natural-numbers

functions: kdf1/1, kdf2/1, kdf3/1, aead/4, xaead/4, decrypt/3, xdecrypt/3, checkaead/4[destructor], checkxaead/4[destructor], mac/2, checkmac/3[destructor], true/0

equations: decrypt(k, n, aead(k, n, a, p)) = p
equations: checkaead(k, n, a, aead(k, n, a, p)) = true
equations: xdecrypt(k, n, xaead(k, n, a, p)) = p
equations: checkxaead(k, n, a, xaead(k, n, a, p)) = true
equations: checkmac(k, m, mac(k,m)) = true

heuristic: s

let RevealPsk(psk) =
    event RevPsk(psk); out(psk)

//let RevealLtki(ltki) =
//    event RevLtki('g'^ltki); out(ltki)

//let RevealLtkr(ltkr) =
//    event RevLtkr('g'^ltkr); out(ltkr)

//let RevealEki(eki) =
//    event RevEki('g'^eki); out(eki)

//let RevealEkin(ekin) =
//    event RevEkin('g'^ekin); out(ekin)

//let RevealEkr(ekr) =
//    event RevEkr('g'^ekr); out(ekr)

//let RevealPre(ltkr, ltki)=
//    event RevPre('g'^ltkr, 'g'^ltki); out(('g'^ltkr)^ltki)

let Initiator(~ltkI, pkI, pkR, ~psk, empty, zero_1) =

    new ~ekI;
    new ~ts;
    new sidI;
    new ~firstI;
    new ~messageI1;
    //new ~messageI2;

    let ctrI0:nat = zero_1 in
    let ctrI1:nat = ctrI0 %+ %1 in
    //let ctrI2:nat = ctrI1 + 'one' in
    let ctrR0:nat = zero_1 in
    //let ctrR1:nat = ctrR0 + 'one' in

    // pkr input

    let sisr = (pkR)^~ltkI in // precompi input

    //InitHello generation
    let pekI  = 'g'^~ekI in
    let ck=h('noise') in


    (   // This parallelizes ekI compromise

      let eisr  = pkR^~ekI in
      let hi0   = h(<ck, 'id'>) in
      let hi1   = h(<hi0, pkR>) in
      let ci0   = kdf1(<ck, pekI>) in
      let hi2   = h(<hi1, pekI>) in
      let ci1   = kdf1(<ci0, eisr>) in
      let ki1   = kdf2(<ci0, eisr>) in
      //event I_SK1(ki1, pkI, pkR, pekI);
      let astat = aead(ki1, zero_1, hi2, pkI) in
      let hi3   = h(<hi2, astat>) in
      let ci2 = kdf1(<ci1, sisr>) in
      let ki2 = kdf2(<ci1, sisr>) in
      event I_SK2(ki2, pkI, pkR, pekI);

      //event I_Time(~ts, pkI, pkR, pekI);
      let ats = aead(ki2, zero_1, hi3, ~ts) in
      let hi4   = h(<hi3, ats>) in

      let kmacI1 = h(<'noise', pkR>) in
      let macI1 = mac(kmacI1, <sidI, pekI, astat, ats>) in
      event ISend(pkI, pkR, pekI, ki1, ki2);
      out(<'xI', sidI, pekI, astat, ats, macI1>);    // Send InitHello

      // RecHello reception

      let kmacR = h(<'noise', pkI>) in        // mac simulation
      in(<'xR', sidIR, sidR, pekR, aempt, macR>);   // Receive RecHello

      if((checkmac(kmacR, <sidIR, sidR, pekR, aempt>, macR)=true) & (not (pekR = 'g'))) then
      (
          if(sidIR=sidI) then
              (
                  let eier  = pekR^~ekI in
                  let sier  = pekR^~ltkI in
                  let ci3   = kdf1(<ci2, pekR>) in
                  let hi5   = h(<hi4, pekR>) in
                  let ci4   = kdf1(<ci3, eier>) in
                  let ci5   = kdf1(<ci4, sier>) in
                  let ci6   = kdf1(<ci5, ~psk>) in
                  let hit   = kdf2(<ci5, ~psk>) in
                  let ki6   = kdf3(<ci5, ~psk>) in
                  //event I_SK6(ki6, pkI, pkR, pekI, pekR, ~psk);
                  let hi6   = h(<hi5, hit>) in

                  let dempty = decrypt(ki6, zero_1, aempt) in

                  if((checkaead(ki6, zero_1, hi6, aempt) = true) & (dempty=empty)) then
                      (
                      let hi7 = h(<hi6, aempt>) in

                      event I_SK6(ki6, pkI, pkR, pekI, pekR, ~psk);
                      event IKeys(pkI, pkR, pekI, pekR, ~psk, ki6);

                      //Key derivation
                      // keyItoR is for data I->R
                      // keyRtoI is for data R->I

                      let keyItoR = kdf1(ki6) in
                      event I_SK_ItoR(keyItoR, pkI, pkR, pekI, pekR, ~psk);
                      let keyRtoI = kdf2(ki6) in
                      event I_SK_RtoI(keyRtoI, pkI, pkR, pekI, pekR, ~psk);

                      // first transport message shall be I->R
                      let confirmI = aead(keyItoR, ctrI0, empty, ~firstI) in
                      event IConfirm(pkI, pkR, pekI, pekR, ~psk, keyItoR);
                      out(<'xTi0', sidR, ctrI0, confirmI>);

                      // then transport messages can be I->R or R->I

                      (
                      let messI1 = aead(keyItoR, ctrI1, empty, ~messageI1) in
                      event ITranspSend1(pkI, pkR, pekI, pekR, ~psk, keyItoR);
                      out(<'xTi1', sidR, ctrI1, messI1>)//;

                      //let messI2 = aead(keyItoR, ctrI2, empty, ~messageI2) in
                      //event ITranspSend2(pkI, pkR, pekI, pekR, ~psk, keyItoR);
                      //out(<'xTi2', sidR, ctrI2, messI2>)
                      )
                      |
                      (
                      in(<'xTr0', sidIR0, cR0:nat, messR0>);
                      if ((sidIR0 = sidI) & (cR0 = ctrR0)) then
                      (
                      let dmessR0 = decrypt(keyRtoI, cR0, messR0) in
                      if(checkaead(keyRtoI, cR0, empty, messR0) = true) then
                        (
                          event ITranspRec0(pkI, pkR, pekI, pekR, ~psk, keyRtoI)//;

                            //in(<'xTr1', sidIR1, cR1:nat, messR1>);
                            //if ((sidIR1 = sidI) & (cR1 = ctrR1)) then
                            //(
                            //let dmessR1 = decrypt(keyRtoI, cR1, messR1) in
                            //if(checkaead(keyRtoI, cR1, empty, messR1) = true) then
                            //  (
                            //    event ITranspRec1(pkI, pkR, pekI, pekR, ~psk, keyRtoI)
                            //  ) // chekaead
                            //)  // sidI22 = sidI & cR22 = ctrR +1+1

                        ) // chekaead
                      )  // sidI2 = sidI & cR2 = ctrR +1
                      ) // Transport message
                      ) // chekaead & dempty = empty
             ) // sidIR = sidI
      ) // checkmac
    )     // This parallelizes ekI compromise
    //| (RevealEki(~ekI))


let Responder(~ltkR, pkR, ~psk, empty, zero_1) =

    new ~ekR;
    new sidR;
    new ~messageR0;
    //new ~messageR1;

    let ctrI0:nat = zero_1 in
    let ctrI1:nat = ctrI0 %+ %1 in
    //let ctrI2:nat = ctrI1 + 'one' in
    let ctrR0:nat = zero_1 in
    //let ctrR1:nat = ctrR0 + 'one' in

    in(<'initiator', pkI>); // pki input

    let srsi = (pkI)^~ltkR in // precompr input

    let pekR  = 'g'^~ekR in

    ( // This parallelizes ekR compromise
    // InitHello reception
    let ck = h('noise') in

    let kmacI1 = h(<'noise', pkR>) in        // mac simulation
    in(<'xI', sidI, pekI, astat, ats, macI1>);     // Receive InitHello
    new stp1;
    event Test(stp1, <'xI', sidI, pekI, astat, ats, macI1>);

    if((checkmac(kmacI1, <sidI, pekI, astat, ats>, macI1) = true) & (not (pekI = 'g'))) then
    (

        let eisr  = pekI^~ltkR in
        let hr0 = h(<ck, 'id'>) in
        let hr1 = h(<hr0, pkR>) in
        let cr0   = kdf1(<ck, pekI>) in
        let hr2   = h(<hr1, pekI>) in
        let cr1   = kdf1(<cr0, eisr>) in
        let kr1   = kdf2(<cr0, eisr>) in
        //event R_SK1(kr1, pkI, pkR, pekI);

        let dpki = decrypt(kr1, zero_1, astat) in
        if((checkaead(kr1, zero_1, hr2, astat) = true) & (dpki=pkI)) then
            (
            let hr3   = h(<hr2, astat>) in
            let cr2   = kdf1(<cr1, srsi>) in
            let kr2   = kdf2(<cr1, srsi>) in
            event R_SK2(kr2, pkI, pkR, pekI);

            let dtime = decrypt(kr2, zero_1, ats) in

            if(checkaead(kr2, zero_1, hr3, ats) = true) then
                (
                //event R_Time(dtime, pkI, pkR, pekI);
                let hr4 = h(<hr3, ats>) in
                event RRec(pkI, pkR, pekI, kr1, kr2);

                // Rechello generation

                let eier  = pekI^~ekR in
                let sier  = pkI^~ekR in

                let cr3 = kdf1(<cr2, pekR>) in
                let hr5 = h(<hr4, pekR>) in
                let cr4 = kdf1(<cr3, eier>) in
                let cr5 = kdf1(<cr4, sier>) in
                let cr6 = kdf1(<cr5, ~psk>) in
                let hrt = kdf2(<cr5, ~psk>) in
                let kr6 = kdf3(<cr5, ~psk>) in
                event R_SK6(kr6, pkI, pkR, pekI, pekR, ~psk);
                let hr6 = h(<hr5, hrt>) in
                let aempt = aead(kr6, zero_1, hr6, empty) in
                let hr7 = h(<hr6, aempt>) in

                let kmacR = h(<'noise', pkI>) in        // mac key
                let macR = mac(kmacR, <sidI, sidR, pekR, aempt>) in

                event RKeys(pkI, pkR, pekI, pekR, ~psk, kr6);
                out(<'xR', sidI, sidR, pekR, aempt, macR>);  // Send RecHello

                // Key derivation
                // keyItoR is for data I->R
                // keyRtoI is for data R->I

                let keyItoR = kdf1(kr6) in
                event R_SK_ItoR(keyItoR, pkI, pkR, pekI, pekR, ~psk);
                let keyRtoI = kdf2(kr6) in
                event R_SK_RtoI(keyRtoI, pkI, pkR, pekI, pekR, ~psk);

                // receive first message I->R
                in(<'xTi0', sidC, cI0:nat, confirmI>);

                if ((sidC = sidR) & (cI0 = ctrI0)) then // true

                    (

                    let dfirstI = decrypt(keyItoR, cI0, confirmI) in
                    if(checkaead(keyItoR, cI0, empty, confirmI) = true) then
                        (
                            event RConfirm(pkI, pkR, pekI, pekR, ~psk, keyItoR);

                            // then transport messages can be I->R or R->I
                            (
                            in(<'xTi1', sidRI1, cI1:nat, messI1>);
                            if ((sidRI1 = sidR) & (cI1 = ctrI1)) then
                            (
                            let dmessI1 = decrypt(keyItoR, cI1, messI1) in
                            if(checkaead(keyItoR, cI1, empty, messI1) = true) then
                            (
                                event RTranspRec1(pkI, pkR, pekI, pekR, ~psk, keyItoR)//;

                                //in(<'xTi2', sidR2, cI2:nat, messI2>);
                                //if ((sidR2 = sidR) & (cI2 = ctrI2)) then
                                //(
                                //let dmessI2 = decrypt(keyItoR, cI2, messI2) in
                                //if(checkaead(keyItoR, cI2, empty, messI2) = true) then
                                //(
                                //event RTranspRec2(pkI, pkR, pekI, pekR, ~psk, keyItoR)
                                //)
                                //)

                            )
                            )
                            )

                            |

                            (
                            let messR0 = aead(keyRtoI, ctrR0, empty, ~messageR0) in
                            event RTranspSend0(pkI, pkR, pekI, pekR, ~psk, keyRtoI);
                            out(<'xTr0', sidI, ctrR0, messR0>)//; // send TransData R->I

                            //let messR1 = aead(keyRtoI, ctrR1, empty, ~messageR1) in
                            //event RTranspSend1(pkI, pkR, pekI, pekR, ~psk, keyRtoI);
                            //out(<'xTr1', sidI, ctrR1, messR1>) // send TransData R->I
                            )

                        )

                    )    // (sidRI1 = sidR) & (cI1 = incr(ctrI)

                )   // checkaead

              )   // checkaead
        )   // checkmac1
    ) // This parallelizes ekR compromise

  //| (RevealEkr(~ekR))


process:

new ~ltkI;
new ~ltkR;
new ~psk;

new empty;

out(<'initiator', 'g'^~ltkI>);
out(<'responder', 'g'^~ltkR>);
out(empty);

// precompi output

// precompr output


(
      (! Initiator(~ltkI, 'g'^~ltkI, 'g'^~ltkR, ~psk, empty, %1))
    | (! Responder(~ltkR, 'g'^~ltkR, ~psk, empty, %1))
    | RevealPsk(~psk)
    //| RevealLtki(~ltkI)
    //| RevealLtkr(~ltkR)
    //| RevealPre(~ltkI, ~ltkR)

)



lemma Agreement_Confirm:
all-traces
"All #i pki pkr peki pekr psk ck.
(RConfirm(pki, pkr, peki, pekr, psk, ck)@i )
==> (Ex #j2. (RevPsk(psk) @ #j2)) | (Ex #j1 . (IConfirm(pki, pkr, peki, pekr, psk, ck)@j1))
"



end

// vim: ft=spthy
