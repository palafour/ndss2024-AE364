#!/bin/sh

eval $(opam env)

JOBS=6

cd secrecy_isk6_pfs

echo "Evaluate ProVerif queries for isk6 pfs"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command