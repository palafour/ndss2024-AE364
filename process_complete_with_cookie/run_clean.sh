#!/bin/sh

cd ../queries

rm -f *.spthy

cd ../process_complete_with_cookie

rm -f wireguard_macro.spthy
rm -R -f results
rm -R -f agreement_inithello
rm -R -f agreement_confirm
rm -R -f agreement_rechello
rm -R -f agreement_transport_itor
rm -R -f agreement_transport_rtoi
rm -R -f secrecy_isk6
rm -R -f secrecy_isk6_pfs
rm -R -f secrecy_rsk6
rm -R -f secrecy_rsk6_pfs
rm -R -f secrecy_isk_itor
rm -R -f secrecy_isk_rtoi
rm -R -f secrecy_rsk_itor
rm -R -f secrecy_rsk_rtoi
rm -R -f secrecy_isk_itor_pfs
rm -R -f secrecy_isk_rtoi_pfs
rm -R -f secrecy_rsk_itor_pfs
rm -R -f secrecy_rsk_rtoi_pfs
