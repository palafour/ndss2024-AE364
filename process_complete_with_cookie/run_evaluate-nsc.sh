#!/bin/sh

eval $(opam env)

JOBS=64

cd __conditions__

echo "Evaluate Necessary and Sufficient Conditions"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command
