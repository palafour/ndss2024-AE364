FROM ubuntu:latest

USER root

WORKDIR /data

RUN mkdir process_complete_mininal_tests && \
    mkdir queries && mkdir process_empty && \
    mkdir process_complete_with_cookie && \ 
    mkdir process_complete_with_fix_guv && \ 
    mkdir process_complete_with_fix_psk && \
    mkdir process_complete_without_cookie && \
    mkdir process_empty_with_cookie

COPY process_complete_mininal_tests process_complete_mininal_tests
COPY queries queries
COPY process_empty process_empty
COPY process_empty_with_cookie process_empty_with_cookie
COPY process_complete_with_fix_guv process_complete_with_fix_guv
COPY process_complete_with_fix_psk process_complete_with_fix_psk
COPY process_complete_without_cookie process_complete_without_cookie
COPY process_empty_with_cookie process_empty_with_cookie

RUN apt update -y  
RUN apt full-upgrade -y
RUN apt install software-properties-common -y
RUN add-apt-repository ppa:avsm/ppa
RUN apt install time libgtk2.0-dev pkg-config opam curl make unzip graphviz build-essential bubblewrap parallel python3-pip locales -y

RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV PATH=/root/.local/bin/:$PATH

RUN sysctl -w net.ipv6.conf.all.disable_ipv6=1 &&  sysctl -w net.ipv6.conf.default.disable_ipv6=1 &&  sysctl -w net.ipv6.conf.lo.disable_ipv6=1

RUN pip3 install sympy

RUN curl -sSL https://get.haskellstack.org/ | sh -s - -f

RUN curl -L https://github.com/SRI-CSL/Maude/releases/download/Maude3.3.1/Maude-linux.zip > maude.zip && \
    unzip -oj maude.zip -d ./maude && \
    mkdir -p /usr/local/bin && \
    mv -f ./maude/maude.linux64 /usr/local/bin/maude && \
    mv -f ./maude/prelude.maude /usr/local/bin/prelude.maude && \
    chmod a+x /usr/local/bin/maude && \
    rm maude.zip

RUN git clone https://github.com/tamarin-prover/tamarin-prover.git &&  \
    cd tamarin-prover && \
    git checkout master && \
    make default && \
    cd ..

RUN opam init --disable-sandboxing && \
    opam install -y proverif

RUN rm -R maude && \
    rm -R tamarin-prover
