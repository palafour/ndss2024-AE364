#!/bin/sh

eval $(opam env)

JOBS=250

cd agreement_inithello

echo "Evaluate ProVerif queries for inithello agreement"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../agreement_rechello

echo "Evaluate ProVerif queries for rechello agreement"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../agreement_confirm

echo "Evaluate ProVerif queries for confirm agreement"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../agreement_transport_itor

echo "Evaluate ProVerif queries for transport itor agreement"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../agreement_transport_rtoi

echo "Evaluate ProVerif queries for transport rtoi agreement"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_isk6

echo "Evaluate ProVerif queries for isk6 secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_rsk6

echo "Evaluate ProVerif queries for rsk6 secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_isk_itor

echo "Evaluate ProVerif queries for isk itor secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_isk_rtoi

echo "Evaluate ProVerif queries for isk rtoi secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_rsk_itor

echo "Evaluate ProVerif queries for rsk itor secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command

cd ../secrecy_rsk_rtoi

echo "Evaluate ProVerif queries for rsk rtoi secrecy"

time -p -f "%E" parallel --jobs $JOBS < wireguard_command