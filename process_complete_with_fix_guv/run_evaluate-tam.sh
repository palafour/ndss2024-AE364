#!/bin/sh

chmod 755 __tamarin__/myoracle

echo "Evaluate Tamarin Lemma"

JOBS=250

time -p -f "%E" parallel --jobs $JOBS < wireguard_command_evaluate_tam
