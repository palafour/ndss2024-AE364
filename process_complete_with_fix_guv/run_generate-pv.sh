#!/bin/sh

python3 __scripts__/generate_macro.py

JOBS=250

echo "Generate ProVerif queries"

cd ../queries

sh run_generate-export-queries.sh

cd ../process_complete_with_fix_guv

echo "Generate ProVerif files"

#time -p -f "%E" parallel --jobs $JOBS < wireguard_command_generate_pv
parallel --jobs $JOBS < wireguard_command_generate_pv

rm -f wireguard_macro.spthy