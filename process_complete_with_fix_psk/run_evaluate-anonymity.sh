#!/bin/sh

eval $(opam env)

cd __anonymity__

rm -f wireguard_command
rm -f *.pv
rm -f *.pv.log

python3 generate_pv_anonymity.py

JOBS=8

parallel --jobs $JOBS < wireguard_command
