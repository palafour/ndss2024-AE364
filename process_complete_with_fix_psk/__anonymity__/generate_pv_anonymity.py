#!/usr/bin/env python3.6

import os

from subprocess import PIPE, Popen, DEVNULL
TAMARIN="~/.local/bin/tamarin-prover"

REVEAL_SET = ["WITHOUT_R", "Ru", "Rv", "Rc", "Rs", "Rx", "Ry", "RcRy", "RuRy"]
file_spthy = "Anonymity_with_fix_psk.spthy"

def update_files(file_to_read, file_to_write):

	with open(file_to_read, 'r') as infile:
		filedata = infile.read()
		filedata = filedata.replace("type nat.\n", "")

	with open(file_to_write, 'w') as outfile:
		outfile.write(filedata)


def tamarin_proverif_anonymity(file_spthy_name, DTAG):
	generated_proverif_name = file_spthy_name[0:-6] + "_" + DTAG
	command = TAMARIN+" "+file_spthy_name+" -D="+DTAG+" +RTS -N1 -RTS -m=proverifequiv > "+generated_proverif_name+".p"
	process = Popen(command, stdout=PIPE, stderr=None, shell=True)
	output = process.communicate()[0]

	update_files(generated_proverif_name+'.p', generated_proverif_name+'.pv')

	os.remove(generated_proverif_name+'.p')

	with open("wireguard_command", "a") as outfile:
		outfile.write("proverif"+" "+generated_proverif_name+".pv"+" > "+generated_proverif_name+".pv.log"+"\n")


for r in REVEAL_SET :
	tamarin_proverif_anonymity(file_spthy, r)


#JOBS=4

#parallel --jobs $JOBS < wireguard_command_evaluate_anonymity
