#!/usr/bin/env python3.6

import itertools


def delelmt(L, e):
    for index in range(len(L)):
        if (L[index] == e):
            del L[index]
            return L


with open('wireguard_agreement_inithello_export_queries.spthy', 'w') as f:

	keys = ["ltki", "ltkr", "pki", "pkr", "eki", "precomp_i", "precomp_r"]

	L1c = list(itertools.combinations(keys, 1))
	L2c = list(itertools.combinations(keys, 2))
	L3c = list(itertools.combinations(keys, 3))
	L4c = list(itertools.combinations(keys, 4))
	L5c = list(itertools.combinations(keys, 5))
	L6c = list(itertools.combinations(keys, 6))
	L7c = list(itertools.combinations(keys, 7))

	
	subkeys = ["ltki", "ltkr", "eki", "precomp_i"]

	S1c = list(itertools.combinations(subkeys, 1))
	S2c = list(itertools.combinations(subkeys, 2))
	S3c = list(itertools.combinations(subkeys, 3))
	S4c = list(itertools.combinations(subkeys, 4))


	f.write('#ifdef all_trusted'+'\n\n')
	f.write('export queries:'+'\n\n')
	f.write('"'+'\n\n')
	#f.write('noselect x:bitstring; attacker(exp(g,*x)).'+'\n\n')
	#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
	#f.write('noselect x:bitstring; mess(new StateChannel, x) [ignoreAFewTimes].'+'\n\n')
	#f.write('noselect x:bitstring; mess(new StateChannel_1, x) [ignoreAFewTimes].'+'\n\n')
	#f.write('noselect x:bitstring; mess(new StateChannel_2, x) [ignoreAFewTimes].'+'\n\n')
	#f.write('noselect x:bitstring; mess(new StateChannel_3, x) [ignoreAFewTimes].'+'\n\n')


	f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
	f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
	f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


	for l in S1c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
		f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
		f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

	for l in S2c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
		f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
		f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

	for l in S3c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
		f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
		f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

	for l in S4c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
		f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
		f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')


	f.write('"'+'\n\n')
	f.write('#endif'+'\n\n')


	for u in L1c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		D6c = list(itertools.combinations(dkeys, 6))
		

		if(u[0] != "precomp_r"):
			f.write('#ifdef untrusted_'+u[0]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))||((event(eRevPri('+l[5]+'))@j6) && (j6 < i))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))||((event(eRevPri('+l[5]+'))@j6) && (j6 < i))).'+'\n\n')

			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

#################
	
	for u in L2c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if((l[0] != "precomp_r") and (l[0] != "pki") and (l[0] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[0] != "pki") and (l[1] != "pki") and (l[0] != "pkr") and (l[1] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if((l[0] != "precomp_i") and (l[0] != "pki") and (l[0] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[0] != "pki") and (l[1] != "pki") and (l[0] != "pkr") and (l[1] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
					f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
					f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))||((event(eRevPri('+l[4]+'))@j5) && (j5 < i))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

#####################
	
	for u in L3c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))||((event(eRevPri('+l[3]+'))@j4) && (j4 < i))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

###############

	for u in L4c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))||((event(eRevPri('+l[2]+'))@j3) && (j3 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L5c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time, j2:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))||((event(eRevPri('+l[1]+'))@j2) && (j2 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L6c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		dkeys = delelmt(dkeys, u[5])
		D1c = list(itertools.combinations(dkeys, 1))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r") and (u[5] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
			#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
			#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
			f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
			f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring, j1:time;'+'\n')
							f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
							f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))||((event(eRevPri('+l[0]+'))@j1) && (j1 < i))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')



	for u in L7c :
		f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'\n\n')
		f.write('export queries:'+'\n\n')
		f.write('"'+'\n\n')
		#f.write('noselect x:bitstring; attacker(exp(g,x)).'+'\n\n')
		#f.write('axiom x: bitstring, y: bitstring, z: bitstring; event(Test(x, y)) && event(Test(x, z)) ==> y = z.'+'\n\n')
		#f.write('noselect x:bitstring; mess(new StateChannel, *x) [ignoreAFewTimes].'+'\n\n')
		#f.write('noselect x:bitstring; mess(new StateChannel_1, *x) [ignoreAFewTimes].'+'\n\n')
		#f.write('noselect x:bitstring; mess(new StateChannel_2, *x) [ignoreAFewTimes].'+'\n\n')
		#f.write('noselect x:bitstring; mess(new StateChannel_3, *x) [ignoreAFewTimes].'+'\n\n')

		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, k1:bitstring, k2:bitstring;'+'\n')
		f.write('(event(eRRec(pki, pkr, peki, k1, k2))@i)'+'\n')
		f.write('==> (((event(eISend(pki, pkr, peki, k1, k2))@j) && (j < i))).'+'\n\n')
		f.write('"'+'\n\n')
		f.write('#endif'+'\n\n')
	


with open('wireguard_agreement_inithello_export_queries.spthy', 'r') as f:
	fdata = f.read()
fdata = fdata.replace('eRevPri(psk', 'eRevPsk(psk')
fdata = fdata.replace('eRevPri(ltki', 'eRevLtki(pki')
fdata = fdata.replace('eRevPri(ltkr', 'eRevLtkr(pkr')
fdata = fdata.replace('eRevPri(ekr', 'eRevEkr(pekr')
fdata = fdata.replace('eRevPri(eki', 'eRevEki(peki')
fdata = fdata.replace('eRevPri(precomp_i', 'eRevPre(pki, pkr')



with open('wireguard_agreement_inithello_export_queries.spthy', 'w') as f:
	f.write(fdata)