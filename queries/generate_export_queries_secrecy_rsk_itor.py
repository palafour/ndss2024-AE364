#!/usr/bin/env python3.6

import itertools


def delelmt(L, e):
    for index in range(len(L)):
        if (L[index] == e):
            del L[index]
            return L


with open('wireguard_secrecy_rsk_itor_export_queries.spthy', 'w') as f:

	keys = ["ltki", "ltkr", "pki", "pkr", "eki", "ekr", "precomp_i", "precomp_r", "psk"]

	L1c = list(itertools.combinations(keys, 1))
	L2c = list(itertools.combinations(keys, 2))
	L3c = list(itertools.combinations(keys, 3))
	L4c = list(itertools.combinations(keys, 4))
	L5c = list(itertools.combinations(keys, 5))
	L6c = list(itertools.combinations(keys, 6))
	L7c = list(itertools.combinations(keys, 7))
	L8c = list(itertools.combinations(keys, 8))
	L9c = list(itertools.combinations(keys, 9))

	
	subkeys = ["ltki", "ltkr", "eki", "ekr", "precomp_i", "psk"]

	S1c = list(itertools.combinations(subkeys, 1))
	S2c = list(itertools.combinations(subkeys, 2))
	S3c = list(itertools.combinations(subkeys, 3))
	S4c = list(itertools.combinations(subkeys, 4))
	S5c = list(itertools.combinations(subkeys, 5))
	S6c = list(itertools.combinations(subkeys, 6))


	f.write('#ifdef all_trusted'+'\n\n')
	f.write('export queries:'+'\n\n')
	f.write('"'+'\n\n')
	#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

	f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
	f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


	for l in S1c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

	for l in S2c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

	for l in S3c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

	for l in S4c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

	for l in S5c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

	for l in S6c :
		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
		f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

	f.write('"'+'\n\n')
	f.write('#endif'+'\n\n')


	for u in L1c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		D6c = list(itertools.combinations(dkeys, 6))
		D7c = list(itertools.combinations(dkeys, 7))
		D8c = list(itertools.combinations(dkeys, 8))
		

		if(u[0] != "precomp_r"):
			f.write('#ifdef untrusted_'+u[0]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

			for l in D7c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r") and (l[6] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))).'+'\n\n')

			for l in D8c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r") and (l[6] != "precomp_r") and (l[7] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki") and (l[7] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr") and (l[7] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time, j8:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))||((event(eRevPri('+l[7]+'))@j8) && (j8 < j))).'+'\n\n')

			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

			for l in D7c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i") and (l[6] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))).'+'\n\n')

			for l in D8c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i") and (l[6] != "precomp_i") and (l[7] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki") and (l[7] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr") and (l[7] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time, j8:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))||((event(eRevPri('+l[7]+'))@j8) && (j8 < j))).'+'\n\n')
			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

#################
	
	for u in L2c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		D6c = list(itertools.combinations(dkeys, 6))
		D7c = list(itertools.combinations(dkeys, 7))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if((l[0] != "precomp_r") and (l[0] != "pki") and (l[0] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[0] != "pki") and (l[1] != "pki") and (l[0] != "pkr") and (l[1] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

			for l in D7c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r") and (l[6] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if((l[0] != "precomp_i") and (l[0] != "pki") and (l[0] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[0] != "pki") and (l[1] != "pki") and (l[0] != "pkr") and (l[1] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
					f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
					f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
					f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

			for l in D7c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i") and (l[6] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki") and (l[6] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr") and (l[6] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time, j7:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j4) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))||((event(eRevPri('+l[6]+'))@j7) && (j7 < j))).'+'\n\n')

			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

#####################
	
	for u in L3c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		D6c = list(itertools.combinations(dkeys, 6))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r") and (l[5] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')

			for l in D6c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i") and (l[5] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki") and (l[5] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr") and (l[5] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time, j6:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))||((event(eRevPri('+l[5]+'))@j6) && (j6 < j))).'+'\n\n')

			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')

###############

	for u in L4c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		D5c = list(itertools.combinations(dkeys, 5))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r") and (l[4] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')

			for l in D5c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i") and (l[4] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki") and (l[4] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr") and (l[4] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time, j5:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))||((event(eRevPri('+l[4]+'))@j5) && (j5 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L5c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		D4c = list(itertools.combinations(dkeys, 4))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r") and (l[3] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')

			for l in D4c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i") and (l[3] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki") and (l[3] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr") and (l[3] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time, j4:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))||((event(eRevPri('+l[3]+'))@j4) && (j4 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L6c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		dkeys = delelmt(dkeys, u[5])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		D3c = list(itertools.combinations(dkeys, 3))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r") and (u[5] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r") and (l[2] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')

			for l in D3c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i") and (l[2] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki") and (l[2] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr") and (l[2] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time, j2:time, j3:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))||((event(eRevPri('+l[2]+'))@j3) && (j3 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L7c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		dkeys = delelmt(dkeys, u[5])
		dkeys = delelmt(dkeys, u[6])
		D1c = list(itertools.combinations(dkeys, 1))
		D2c = list(itertools.combinations(dkeys, 2))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r") and (u[5] != "precomp_r") and (u[6] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_r") and (l[1] != "precomp_r")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')

			for l in D2c :
				if((l[0] != "precomp_i") and (l[1] != "precomp_i")):
					if((l[0] != "pki") and (l[1] != "pki")):
						if((l[0] != "pkr") and (l[1] != "pkr")):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, i1:time, j1:time, j2:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))||((event(eRevPri('+l[1]+'))@j2) && (j2 < j))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


###############

	for u in L8c :
		keys_in = keys.copy()
		dkeys = delelmt(keys_in, u[0])
		dkeys = delelmt(dkeys, u[1])
		dkeys = delelmt(dkeys, u[2])
		dkeys = delelmt(dkeys, u[3])
		dkeys = delelmt(dkeys, u[4])
		dkeys = delelmt(dkeys, u[5])
		dkeys = delelmt(dkeys, u[6])
		dkeys = delelmt(dkeys, u[7])
		D1c = list(itertools.combinations(dkeys, 1))
		

		if((u[0] != "precomp_r") and (u[1] != "precomp_r") and (u[2] != "precomp_r") and (u[3] != "precomp_r") and (u[4] != "precomp_r") and (u[5] != "precomp_r") and (u[6] != "precomp_r") and (u[7] != "precomp_r")):
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'_'+u[7]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_r"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')



			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')
		else:
			f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'_'+u[7]+'\n\n')
			f.write('export queries:'+'\n\n')
			f.write('"'+'\n\n')
			#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

			f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
			f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')


			for l in D1c :
				if(l[0] != "precomp_i"):
					if(l[0] != "pki"):
						if(l[0] != "pkr"):
							f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring, j1:time;'+'\n')
							f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j)'+'\n')
							f.write('==> (((event(eRevPri('+l[0]+'))@j1) && (j1 < j))).'+'\n\n')


			f.write('"'+'\n\n')
			f.write('#endif'+'\n\n')


	for u in L9c :
		f.write('#ifdef untrusted_'+u[0]+'_'+u[1]+'_'+u[2]+'_'+u[3]+'_'+u[4]+'_'+u[5]+'_'+u[6]+'_'+u[7]+'_'+u[8]+'\n\n')
		f.write('export queries:'+'\n\n')
		f.write('"'+'\n\n')
		#f.write('nounif x:bitstring; attacker(exp(g,x)).'+'\n\n')

		f.write('query i:time,j:time, pki:bitstring, pkr:bitstring, peki:bitstring, pekr:bitstring, psk:bitstring, ck:bitstring;'+'\n')
		f.write('(event(eR_SK_ItoR(ck, pki, pkr, peki, pekr, psk))@i) && (attacker(ck)@j).'+'\n\n')
		f.write('"'+'\n\n')
		f.write('#endif'+'\n\n')
	


with open('wireguard_secrecy_rsk_itor_export_queries.spthy', 'r') as f:
	fdata = f.read()
fdata = fdata.replace('eRevPri(psk', 'eRevPsk(psk')
fdata = fdata.replace('eRevPri(ltki', 'eRevLtki(pki')
fdata = fdata.replace('eRevPri(ltkr', 'eRevLtkr(pkr')
fdata = fdata.replace('eRevPri(ekr', 'eRevEkr(pekr')
fdata = fdata.replace('eRevPri(eki', 'eRevEki(peki')
fdata = fdata.replace('eRevPri(precomp_i', 'eRevPre(pki, pkr')



with open('wireguard_secrecy_rsk_itor_export_queries.spthy', 'w') as f:
	f.write(fdata)